(function(){

  var $ = $ ? $ : jQuery;
  console.log('header.js');

  var windowResize = function() {
    if( $(window).width() >= 768 ) {
      $('.site-header').addClass('showDesktop').removeClass("showMobile");

    } else {
      $('.site-header').addClass('showMobile').removeClass("showDesktop");
    }

  };

  $(window).resize(function(){
    windowResize();
  });

  $(document).ready(function(){
    windowResize();

    $('.nav--mobile .menuToggle').click(function(e){
      e.preventDefault();

      var $headerMenu = $('.nav--mobile .header-menu');
      if( $headerMenu.hasClass('in') ) {
        $headerMenu.hide().removeClass('in');
      } else {
        $headerMenu.show().addClass('in');
      }
    });

  });

}());