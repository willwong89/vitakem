<?php
  $options = thrive_get_options_for_post( get_the_ID() );
?>

<?php
 ob_start();
?>
<ul class="secondary-menu">
  <li class="secondary-menu-item">
    <!-- <img src="#" width="25" class="secondary-menu-item__icon"> -->
    <div class="secondary-menu-item__content">
      <p>
      	<strong>FDA, NSF, GMP Certified</strong><br>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphic-certified.png" width="75" class="secondary-menu-item__graphic">
      </p>
    </div>
  </li>
  <li class="secondary-menu-item">
    <!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-drop-shipping.png" width="25" class="secondary-menu-item__icon"> -->
    <div class="secondary-menu-item__content">
      <p>
      	<strong>Drop Shipping</strong><br>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphic-drop-shipping.png" width="75" class="secondary-menu-item__graphic">
      </p>
    </div>
  </li>
  <li class="secondary-menu-item">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-phone.png" width="25" class="secondary-menu-item__icon">
    <div class="secondary-menu-item__content">
      <p><strong>Free Quote<br></strong><?php echo $options['header_phone_no']; ?></p>
    </div>
  </li>
</ul>
<?php
  $secondary_menu_html = ob_get_clean();
?>

<div class="header__wrapper" data-section-id="header" data-section-type="header">

	<?php
		/*
	  <div class="info-bar showDesktop">
	    <div class="wrapper text-center">
	      <div class="header-message uppercase">
	          Header message goes here
	      </div>
	    </div>
	  </div>
	  */
  ?>

  <header class="site-header showDesktop" role="banner">

    <div class="site-header__top">
      <div class="wrapper">
      	<?php echo $secondary_menu_html; ?>
      </div>
    </div>

    <div class="site-header__desktop">
      <div class="wrapper">
        <div class="nav--desktop">
          <div class="mobile-wrapper">
					  <div class="logo-wrapper logo-wrapper--image">
				      <h1 class="h4 header-logo" itemscope="" itemtype="http://schema.org/Organization">
				        <a href="<?php echo home_url( '/' ); ?>" itemprop="url">
				          <img
				           src="<?php echo $options['logo']; ?>"
				           alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
				           class="logo-image" itemprop="logo">
				        </a>
				      </h1>
				    </div>
					  <a href="#" class="menuToggle header-hamburger"></a>
					</div>
					<div class="header-menu header-menu--top nav-wrapper">
					  <ul class="main-menu main-menu--top accessibleNav">
				      <li>
      					<?php echo $secondary_menu_html; ?>
						  </li>
					  </ul>
					</div>
        </div>
      </div>
    </div>

    <div class="site-header__mobile">
      <div class="wrapper">
        <div class="nav--mobile">
          <div class="mobile-wrapper">
					  <div class="logo-wrapper logo-wrapper--image">
					      <h1 class="h4 header-logo" itemscope="" itemtype="http://schema.org/Organization">
					        <a href="<?php echo home_url( '/' ); ?>" itemprop="url">
					          <img
					           src="<?php echo $options['logo']; ?>"
					           alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
					           class="logo-image" itemprop="logo">
					        </a>
					      </h1>
					  </div>
					  <a href="#" class="menuToggle header-hamburger"></a>
					</div>
					<div class="header-menu nav-wrapper">

						<?php if ( has_nav_menu( "primary" ) ): ?>
							<?php locate_template( '/inc/templates/woocommerce-navbar-mini-cart.php', true, true ); ?>
							<?php wp_nav_menu( array(
								'container'       => 'nav',
								'depth'           => 0,
								'theme_location'  => 'primary',
								'container_class' => "main-menu accessibleNav",
								'menu_class'      => 'menu',
								'walker'          => new thrive_custom_menu_walker()
							) ); ?>
						<?php else: ?>
							<div class="dfm">
								<?php _e( "Assign a 'primary' menu", 'thrive' ); ?>
							</div>
						<?php endif; ?>

						<?php
						/*
					  <ul class="main-menu accessibleNav">
						  <li class="child  kids-0">
						    <a href="/collections/all" class="nav-link">Shop</a>
						  </li>
						  <li class="child  kids-0">
						    <a href="/pages/about-us" class="nav-link">About</a>
						  </li>
						  <li class="child  kids-0">
						    <a href="/blogs/news" class="nav-link">Blog</a>
						  </li>
						  <li class="child  kids-0">
						    <a href="/pages/contact-us" class="nav-link">Contact Us</a>
						  </li>
					  </ul>
					  */
					  ?>


					</div>
        </div>
      </div>
    </div>

  </header>

</div>
